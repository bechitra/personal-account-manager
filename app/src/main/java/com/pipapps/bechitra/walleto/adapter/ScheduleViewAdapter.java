package com.pipapps.bechitra.walleto.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.pipapps.bechitra.walleto.R;
import com.pipapps.bechitra.walleto.activity.ScheduleManagementActivity;
import com.pipapps.bechitra.walleto.dialog.listener.OnDeleteItem;
import com.pipapps.bechitra.walleto.room.entity.Schedule;
import com.pipapps.bechitra.walleto.utility.DateManager;
import com.pipapps.bechitra.walleto.utility.PreferenceManager;
import com.pipapps.bechitra.walleto.utility.ScheduleParcel;

import java.text.ParseException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleViewAdapter extends RecyclerView.Adapter<ScheduleViewAdapter.DataBinder>{
    Context context;
    List<Schedule> data;
    RelativeLayout.LayoutParams params;

    OnDeleteItem listener;
    private String currency;

    public ScheduleViewAdapter(Context context) {
        this.context = context;
        currency = PreferenceManager.getInstance(context).getCurrencySign();
        this.params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public DataBinder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_schedule_data,  null);
        view.setLayoutParams(params);

        return new DataBinder(view);
    }

    public void setData(List<Schedule> list) {
        this.data = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(DataBinder holder, int position) {
        Schedule schedule = data.get(position);
        holder.categoryText.setText(schedule.getCategory());
        holder.itemAmountText.setText(schedule.getAmount()+""+currency);
        holder.tableName.setText(schedule.getTag());
        holder.lastRepeatDateText.setText(schedule.getDate());

        if(schedule.getRepeat().equals("1"))
            holder.repetitionText.setText("Repeat Tomorrow");
        else {
            DateManager spc = new DateManager();

            String currentDate = spc.getCurrentDate();
            long diff = 0;
            try {
                long interval = Long.parseLong(schedule.getRepeat());
                Log.d("date", interval+" <-");
                diff = spc.getNextInterval(schedule.getDate(), currentDate, interval);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            holder.repetitionText.setText("Repeat within "+diff+" days");
        }

        if(!schedule.isActive()) {
            holder.categoryText.setTextColor(Color.GRAY);
            holder.repetitionText.setTextColor(Color.RED);
            holder.itemAmountText.setTextColor(Color.BLACK);
            holder.repetitionText.setText("Currently Inactive");
        } else {
            holder.categoryText.setTextColor(Color.GREEN);
            holder.repetitionText.setTextColor(Color.BLUE);
            holder.itemAmountText.setTextColor(Color.RED);
        }
    }

    @Override
    public int getItemCount() {
        return (data == null) ? 0 : data.size();
    }

    class DataBinder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.repetitionText) TextView repetitionText;
        @BindView(R.id.lastRepeatDateText) TextView lastRepeatDateText;
        @BindView(R.id.tableName) TextView tableName;
        @BindView(R.id.itemAmountText) TextView itemAmountText;
        @BindView(R.id.categoryText) TextView categoryText;
        @BindView(R.id.relativeLayout) RelativeLayout relativeLayout;

        public DataBinder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ScheduleManagementActivity.class);
            ScheduleParcel parcelSchedule = new ScheduleParcel(data.get(getAdapterPosition()));
            intent.putExtra("schedule", parcelSchedule);
            context.startActivity(intent);
        }
    }

    public void setOnDeleteItemListener(OnDeleteItem listener) {
        this.listener = listener;
    }
}
