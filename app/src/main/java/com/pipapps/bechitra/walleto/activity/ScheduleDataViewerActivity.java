package com.pipapps.bechitra.walleto.activity;
import android.os.Bundle;
import android.view.View;

import com.pipapps.bechitra.walleto.adapter.ScheduleViewAdapter;
import com.pipapps.bechitra.walleto.databinding.ActivityScheduleDataViewerBinding;
import com.pipapps.bechitra.walleto.room.entity.Schedule;
import com.pipapps.bechitra.walleto.viewmodel.ScheduleDataViewerViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class ScheduleDataViewerActivity extends AppCompatActivity {
    private ScheduleViewAdapter adapter;
    ActivityScheduleDataViewerBinding viewBind;
    ScheduleDataViewerViewModel viewModel;
    List<Schedule> scheduleList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBind = ActivityScheduleDataViewerBinding.inflate(getLayoutInflater());
        setContentView(viewBind.getRoot());
        scheduleList = new ArrayList<>();

        viewBind.allScheduleRadioButton.setChecked(true);

        viewModel = new ViewModelProvider(this).get(ScheduleDataViewerViewModel.class);
        viewModel.getAllSchedule().observe(this, schedules -> {

            emptyListSignal(schedules);
            adapter.setData(schedules);
            scheduleList = schedules;
        });

        viewBind.allScheduleRadioButton.setOnClickListener(view -> {
            viewBind.allScheduleRadioButton.setChecked(true);
            viewBind.activeScheduleRadioButton.setChecked(false);
            viewBind.inactiveScheduleRadioButton.setChecked(false);
            adapter.setData(scheduleList);
            emptyListSignal(scheduleList);
        });

        viewBind.activeScheduleRadioButton.setOnClickListener(view -> {
            viewBind.allScheduleRadioButton.setChecked(false);
            viewBind.activeScheduleRadioButton.setChecked(true);
            viewBind.inactiveScheduleRadioButton.setChecked(false);
            List<Schedule> activeSchedules = viewModel.getActiveSchedules(scheduleList);
            adapter.setData(activeSchedules);
            emptyListSignal(activeSchedules);
        });

        viewBind.inactiveScheduleRadioButton.setOnClickListener(view -> {
            viewBind.allScheduleRadioButton.setChecked(false);
            viewBind.activeScheduleRadioButton.setChecked(false);
            viewBind.inactiveScheduleRadioButton.setChecked(true);
            List<Schedule> inActiveSchedules = viewModel.getInActiveSchedules(scheduleList);
            adapter.setData(inActiveSchedules);
            emptyListSignal(inActiveSchedules);
        });

        viewBind.recyclerView.setHasFixedSize(true);
        viewBind.recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        adapter = new ScheduleViewAdapter(this);
        viewBind.recyclerView.setAdapter(adapter);

        viewBind.backButton.setOnClickListener(view -> finish());
    }

    private void emptyListSignal(List<Schedule> schedules) {
        if(!schedules.isEmpty())
            viewBind.emptyScheduleText.setVisibility(View.GONE);
        else
            viewBind.emptyScheduleText.setVisibility(View.VISIBLE);
    }
}
