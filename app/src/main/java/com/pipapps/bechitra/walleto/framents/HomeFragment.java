package com.pipapps.bechitra.walleto.framents;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.github.mikephil.charting.data.PieData;
import com.pipapps.bechitra.walleto.DataRepository;
import com.pipapps.bechitra.walleto.adapter.DataOrganizerAdapter;
import com.pipapps.bechitra.walleto.databinding.FragmentHomeBinding;
import com.pipapps.bechitra.walleto.room.entity.Transaction;
import com.pipapps.bechitra.walleto.utility.PreferenceManager;
import com.pipapps.bechitra.walleto.viewmodel.HomeFragmentViewModel;

import java.util.List;

;

public class HomeFragment extends Fragment{
    DataOrganizerAdapter adapter;
    FragmentHomeBinding binding;
    HomeFragmentViewModel homeFragmentViewModel;
    private List<Transaction> transactions;
    boolean tableFlag = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.mainActivityLayout.setFocusableInTouchMode(true);
        binding.mainActivityLayout.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        homeFragmentViewModel = new ViewModelProvider(requireActivity(), new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new HomeFragmentViewModel(requireActivity().getApplication());
            }
        }).get(HomeFragmentViewModel.class);

        String[] array = {"Spending", "Earning"};
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                (view.getContext(), android.R.layout.simple_spinner_item,
                        array); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        binding.tableSelectorSpinner.setAdapter(spinnerArrayAdapter);

        binding.currentMonthRecycler.setHasFixedSize(true);
        binding.currentMonthRecycler.setLayoutManager(
                new LinearLayoutManager(view.getContext(),
                        LinearLayoutManager.VERTICAL, false));

        homeFragmentViewModel.getActiveWalletData().observe(getViewLifecycleOwner(), wallet -> {
            transactions = homeFragmentViewModel.getListOfTransaction();
            
            if(wallet != null)
                refreshData(transactions, wallet.getBalance());

        });

        adapter = new DataOrganizerAdapter(requireContext());
        binding.currentMonthRecycler.setAdapter(adapter);
        binding.currentMonthRecycler.setNestedScrollingEnabled(false);


        binding.tableSelectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) {
                    tableFlag = false;
                    if(transactions != null) {
                        viewPieChart(homeFragmentViewModel.getTransactionsOfCurrentMonth(transactions, DataRepository.SPENDING_TAG));
                        adapter.setData(homeFragmentViewModel.getDataOrganizerListOfCurrentMonth(transactions, DataRepository.SPENDING_TAG));
                    }
                } else {
                    tableFlag = true;
                    if(transactions != null) {
                        viewPieChart(homeFragmentViewModel.getTransactionsOfCurrentMonth(transactions, DataRepository.EARNING_TAG));
                        adapter.setData(homeFragmentViewModel.getDataOrganizerListOfCurrentMonth(transactions, DataRepository.EARNING_TAG));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    private void refreshData(List<Transaction> transactions, double balance) {
        adapter.setData(homeFragmentViewModel.getDataOrganizerListOfCurrentMonth( transactions,
                (tableFlag) ? DataRepository.EARNING_TAG :DataRepository.SPENDING_TAG));

        viewPieChart(homeFragmentViewModel.getTransactionsOfCurrentMonth(transactions, (tableFlag) ? DataRepository.EARNING_TAG :DataRepository.SPENDING_TAG));

        String currency = PreferenceManager.getInstance(getContext()).getCurrencySign();

        double earning = homeFragmentViewModel.getAmountByTag(transactions, DataRepository.EARNING_TAG);
        double spending = homeFragmentViewModel.getAmountByTag(transactions, DataRepository.SPENDING_TAG);
        binding.earnBalanceText.setText(currency+" "+earning);
        binding.spendBalanceText.setText(currency+" "+spending);
        binding.mainBalance.setText(currency+" "+balance);
    }

    private void viewPieChart(List<Transaction> transactions) {
        PieData pieData = homeFragmentViewModel.getPieData(transactions, binding.halfPieChart.getLegend());
        loadPieScreen();
        binding.halfPieChart.setBackgroundColor(Color.WHITE);
        binding.halfPieChart.setUsePercentValues(true);
        binding.halfPieChart.getDescription().setEnabled(false);
        binding.halfPieChart.setDrawHoleEnabled(true);

        binding.halfPieChart.setRotationAngle(180);
        binding.halfPieChart.animateY(1000);
        binding.halfPieChart.setRotationEnabled(false);
        binding.halfPieChart.setDrawEntryLabels(false);

        binding.halfPieChart.setCenterText(tableFlag ? DataRepository.EARNING_TAG : DataRepository.SPENDING_TAG);
        binding.halfPieChart.setCenterTextSize(13);
        binding.halfPieChart.setTransparentCircleRadius(56f);

        binding.halfPieChart.setData(pieData);
        binding.halfPieChart.invalidate();
    }



    private void loadPieScreen() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)binding.halfPieChart.getLayoutParams();
        params.setMargins(0,0,0,0);
        binding.halfPieChart.setLayoutParams(params);
    }
}
