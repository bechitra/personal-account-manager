package com.pipapps.bechitra.walleto.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    private static PreferenceManager instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private PreferenceManager(Context context)
    {
        preferences = context.getSharedPreferences("money_manager", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static PreferenceManager getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new PreferenceManager(context);
        }

        return instance;
    }

    public void setCurrency(String currency, String sign)
    {
        editor.putString("currency", currency).commit();
        editor.putString("currency_sign", sign).commit();
    }

    public String getCurrency()
    {
        return preferences.getString("currency", "$ USD");
    }

    public String getCurrencySign()
    {
        return preferences.getString("currency_sign", "$");
    }
}
