package com.pipapps.bechitra.walleto.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.pipapps.bechitra.walleto.DataRepository;
import com.pipapps.bechitra.walleto.room.entity.Schedule;
import com.pipapps.bechitra.walleto.utility.DataProcessor;

import java.util.List;

public class ScheduleManagementActivityViewModel extends AndroidViewModel {
    DataRepository repository;
    DataProcessor processor;
    public ScheduleManagementActivityViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
        processor = new DataProcessor();
    }

    public void updateSchedule(Schedule schedule) { repository.updateSchedule(schedule); }
    public void deleteSchedule(Schedule schedule) { repository.deleteSchedule(schedule); }

    public List<String> getDistinctCategory(List<String> category, String tag) {
        return processor.getDistinctCategory(category, repository.getListTransaction(), tag);
    }
}
