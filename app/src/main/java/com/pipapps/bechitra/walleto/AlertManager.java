package com.pipapps.bechitra.walleto;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.pipapps.bechitra.walleto.room.entity.Schedule;
import com.pipapps.bechitra.walleto.room.entity.Transaction;
import com.pipapps.bechitra.walleto.room.entity.Wallet;
import com.pipapps.bechitra.walleto.utility.DateManager;

import java.text.ParseException;
import java.util.List;

public class AlertManager extends BroadcastReceiver{
    DataRepository repository;
    DateManager manager;

    @Override
    public void onReceive(Context context, Intent intent) {
        repository = new DataRepository((Application) context.getApplicationContext());
        manager = new DateManager();
        List<Schedule> schedules = repository.getAllScheduleList();
        boolean flag = false;
        int counter = 0;

        for(Schedule s : schedules) {
            try {
                if(isMatched(s.getDate(), Long.parseLong(s.getRepeat())) && s.isActive()) {
                    Transaction data = new Transaction(
                            s.getCategory(), s.getAmount(), s.getNote(),
                            manager.getCurrentDate(), s.getTag(), s.getWalletID()
                    );

                    s.setDate(manager.getCurrentDate());
                    repository.updateSchedule(s);
                    repository.insertTransaction(data);

                    if(s.getTag().equals(DataRepository.SPENDING_TAG)) {
                        Wallet wallet = repository.getActiveWallet();
                        double balance = wallet.getBalance();
                        wallet.setBalance(balance - data.getAmount());
                        repository.updateWallet(wallet);
                    } else {
                        Wallet wallet = repository.getActiveWallet();
                        double balance = wallet.getBalance();
                        wallet.setBalance(balance + data.getAmount());
                        repository.updateWallet(wallet);
                    }

                    counter++;
                    flag = true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(flag)
                notificationCreator(context, counter);
        }
    }

    public boolean isMatched(String start, long interval) throws ParseException {
        String today = manager.getCurrentDate();
        long difference = Math.abs(manager.dateDifference(start, today)) / interval;
        String date = start;
        for(int i = 0; i <= difference; i++) {
            date = manager.addDate(date, (int)interval);

            if(date.equals(today))
                return true;
        }

        return false;
    }


    private void notificationCreator(Context context, int counter) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent repeat = new Intent(context, MainActivity.class);
        repeat.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 100, repeat, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, null)
                .setSmallIcon(R.drawable.ic_add_spending_black_24dp)
                .setContentTitle("Automatic Data insertion")
                .setContentText(counter+" new record Added to the database")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String id = "my_channel_01";
            CharSequence name = "Char Seq";
            String description = "Hello World";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(id, name, importance);
            mChannel.setDescription(description);

            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);

            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(mChannel);
            builder.setChannelId(id);
        }

        notificationManager.notify(100, builder.build());
    }
}
