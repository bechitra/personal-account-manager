package com.pipapps.bechitra.walleto.activity;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pipapps.bechitra.walleto.DataRepository;
import com.pipapps.bechitra.walleto.adapter.WalletCreatorAdapter;
import com.pipapps.bechitra.walleto.databinding.ActivityAccountManagementBinding;
import com.pipapps.bechitra.walleto.dialog.WalletCreatorDialog;
import com.pipapps.bechitra.walleto.room.entity.Transaction;
import com.pipapps.bechitra.walleto.room.entity.Wallet;
import com.pipapps.bechitra.walleto.utility.PreferenceManager;
import com.pipapps.bechitra.walleto.viewmodel.AccountManagementActivityViewModel;

import java.util.List;

import butterknife.ButterKnife;

public class AccountManagementActivity extends AppCompatActivity {
    Wallet wallet;
    WalletCreatorAdapter adapter;
    List<Wallet> data;
    AccountManagementActivityViewModel viewModel;
    ActivityAccountManagementBinding viewBind;
    private String currency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBind = ActivityAccountManagementBinding.inflate(getLayoutInflater());
        setContentView(viewBind.getRoot());
        ButterKnife.bind(this);
        viewModel = new ViewModelProvider(this).get(AccountManagementActivityViewModel.class);

        adapter = new WalletCreatorAdapter(this);
        currency = PreferenceManager.getInstance(this).getCurrencySign();

        viewBind.recyclerView.setHasFixedSize(true);
        viewBind.recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        viewBind.recyclerView.setAdapter(adapter);

        viewModel.getWalletList().observe(this, walletList -> {
            data = walletList;

            wallet = viewModel.getActiveWallet(walletList);
            adapter.setData(walletList);
            List<Transaction> transactions = viewModel.getAllTransaction();

            double spend = viewModel.getBalanceByTaggedTransaction(transactions, DataRepository.SPENDING_TAG);
            viewBind.spendingAmount.setText(currency+""+spend);
        });

        viewBind.createWalletButton.setOnClickListener(view-> {
            WalletCreatorDialog dialog = new WalletCreatorDialog();
            dialog.show(getSupportFragmentManager(), "OK");
            dialog.setDialogListener((regex, flag) -> {
                        if (flag) {
                            wallet.setActive(false);
                            viewModel.updateWallet(wallet);
                            wallet = new Wallet(regex.toUpperCase(), 0, true);
                            viewModel.insertNewWallet(wallet);
                        } else
                            viewModel.insertNewWallet(new Wallet(regex.toUpperCase(), 0, false));

                    }
            );

        });

        adapter.setOnActiveClickedListener((flag, status) -> {
            Wallet active = wallet;
            wallet = data.get(flag);
            if(active.getId() != wallet.getId()) {
                active.setActive(false);
                viewModel.updateWallet(active);
                wallet.setActive(true);
                viewModel.updateWallet(wallet);
            }
        });

        adapter.setOnDeleteClickedListener((tag, flag) -> {
            Wallet onDeleteWallet = data.get(flag);
            if(!onDeleteWallet.isActive()) {
                viewModel.deleteTransactionByID(onDeleteWallet.getId());
                viewModel.deleteWallet(data.get(flag));
            } else {
                Toast.makeText(getApplicationContext(), "Can't delete running wallet", Toast.LENGTH_SHORT)
                        .show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
