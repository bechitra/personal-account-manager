package com.pipapps.bechitra.walleto.viewmodel;

import android.app.Application;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.pipapps.bechitra.walleto.DataRepository;
import com.pipapps.bechitra.walleto.room.entity.Transaction;
import com.pipapps.bechitra.walleto.room.entity.Wallet;
import com.pipapps.bechitra.walleto.utility.ColorUtility;
import com.pipapps.bechitra.walleto.utility.DataOrganizer;
import com.pipapps.bechitra.walleto.utility.DataProcessor;
import com.pipapps.bechitra.walleto.utility.DateManager;
import com.pipapps.bechitra.walleto.utility.EntrySet;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HomeFragmentViewModel extends AndroidViewModel {
    private DataRepository repository;
    private DataProcessor processor;
    private DateManager manager;
    private String lowerBound, upperBound;
    private ColorUtility utility;
    public HomeFragmentViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
        processor = new DataProcessor();
        manager = new DateManager();
        upperBound = manager.getCurrentDate();
        lowerBound = manager.getFirstDate(upperBound);
        utility = new ColorUtility();
    }

    public Wallet getActiveWallet() { return repository.getActiveWallet(); }

    public List<Transaction> getListOfTransaction() {
        return processor.getTransactionsByRange(repository.getListTransaction(), lowerBound, upperBound);
    }

    public List<DataOrganizer> getDataOrganizerListOfCurrentMonth(List<Transaction> transactions, String tag) {
        return processor.getProcessedTransactionOfCurrentMonth(transactions, tag);
    }

    public List<Transaction> getTransactionsOfCurrentMonth(List<Transaction> transactions, String tag) {
        String upperBound = manager.getCurrentDate();
        String lowerBound = manager.getFirstDate(upperBound);

        return processor.getTransactionsByRange(processor.getTransactionsByTag(transactions, tag), lowerBound, upperBound);

    }

    public LiveData<Wallet> getActiveWalletData() {
        return repository.getActiveWalletData();
    }

    private List<EntrySet> getPieChartData(List<Transaction> data) {
        List<EntrySet> graphData = new ArrayList<>();
        List<String> distinctCategory = new ArrayList<>();

        double totalTransaction = 0;

        if(!data.isEmpty()) {
            for (Transaction t : data) {
                if (distinctCategory.contains(t.getCategory()) && !graphData.isEmpty()) {
                    int categoryIndex = graphData.indexOf(t.getCategory());
                    if(categoryIndex >= 0)
                        graphData.get(categoryIndex).setValue(graphData.get(categoryIndex).getValue() + t.getAmount());
                } else {
                    graphData.add(new EntrySet(t.getCategory(), t.getAmount()));
                    distinctCategory.add(t.getCategory());
                }

                totalTransaction += t.getAmount();
            }

            for(EntrySet entrySet : graphData) {
                int entryIndex = graphData.indexOf(entrySet);
                double transactionRatio = graphData.get(entryIndex).getValue()/totalTransaction;
                graphData.get(entryIndex).setValue(transactionRatio * 100);
            }
        }
        else
            graphData.add(new EntrySet("Empty Chart", 100.0));


        return graphData;
    }

    public double getAmountByTag(List<Transaction> transactions, String tag) {
        return processor.getAmountByTag(transactions, tag);
    }


    public PieData getPieData(List<Transaction> data, Legend pieLegend) {
        ArrayList<PieEntry> values = new ArrayList<>();
        List<EntrySet> entrySets = getPieChartData(data);

        int[]colors = utility.getColorArray(getOrderedCategory(entrySets));
        for(EntrySet entrySet : entrySets)
            values.add(new PieEntry((float) entrySet.getValue(), entrySet.getKey()));

        PieDataSet dataSet = new PieDataSet(values, "");
        dataSet.setSelectionShift(5f);
        dataSet.setSliceSpace(3f);
        dataSet.setAutomaticallyDisableSliceSpacing(false);
        dataSet.setColors(colors);

        PieData pieData = new PieData(dataSet);
        pieData.setValueFormatter(new ValueFormatter(values));
        pieData.setValueTextSize(10f);
        pieData.setValueTextColor(Color.WHITE);
        Legend legend = pieLegend;
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
        legend.setForm(Legend.LegendForm.CIRCLE);

        return pieData;
    }

    public List<String> getOrderedCategory(List<EntrySet> entrySets) {
        List<String> category = new ArrayList<>();

        for(EntrySet entrySet : entrySets)
            category.add(entrySet.getKey());

        return category;
    }

    class ValueFormatter implements IValueFormatter {
        private List<PieEntry> entrySets;
        private DecimalFormat mFormat;
        Set<Float> sets;
        int counter = 0;
        public ValueFormatter(List<PieEntry> entrySets) {
            sets = new HashSet<>();
            this.entrySets = entrySets;
            mFormat = new DecimalFormat("###,###,##0.0");
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            /*PieEntry entrySet = new PieEntry(0);

            for(int i = 0; i < entrySets.size(); i++)
                if(entrySets.get(i).getValue() == entry.getY())
                    entrySet = entrySets.get(i);

             */

            return mFormat.format(value)+" %";
        }
    }
}
