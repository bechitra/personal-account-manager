package com.pipapps.bechitra.walleto.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.pipapps.bechitra.walleto.R;
import com.pipapps.bechitra.walleto.databinding.ActivitySettingsBinding;
import com.pipapps.bechitra.walleto.dialog.ResetDialog;
import com.pipapps.bechitra.walleto.utility.PreferenceManager;
import com.pipapps.bechitra.walleto.viewmodel.SettingActivityViewModel;

public class SettingsActivity extends AppCompatActivity {
    private SettingActivityViewModel viewModel;
    private ActivitySettingsBinding viewBind;

    private String[] currencies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBind = ActivitySettingsBinding.inflate(getLayoutInflater());
        setContentView(viewBind.getRoot());

        currencies = getResources().getStringArray(R.array.currency_list);

        viewModel = new ViewModelProvider(this).get(SettingActivityViewModel.class);

        viewBind.resetWalletSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked) {
                ResetDialog resetDialog = new ResetDialog();
                resetDialog.show(getSupportFragmentManager(), "TAG");
                resetDialog.setOnResetListener(action -> {
                    if(action) {
                        viewModel.reset();
                        toastMassage("Action Successful");
                        viewBind.resetWalletSwitch.setChecked(false);
                    } else {
                        toastMassage("Action not Performed");
                        viewBind.resetWalletSwitch.setChecked(false);
                    }
                });
            }
        });

        viewBind.spinnerCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String currency = currencies[position];
                String sign = currency.split("\\s+")[0];

                PreferenceManager.getInstance(getApplicationContext()).setCurrency(currency, sign);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String currency = PreferenceManager.getInstance(this).getCurrency();
        for (int i = 0; i < currencies.length; i++) {
            if(currencies[i].equalsIgnoreCase(currency))
            {
                viewBind.spinnerCurrency.setSelection(i);
                break;
            }
        }
    }

    private void toastMassage(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
